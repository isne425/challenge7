#include <iostream>
#include "strvar.h"

void conversation(int max_name_size);
//Continues converstation with user

int main()
{
	using namespace std;
	conversation(30);
	cout << "End of Demo.\n";
	return 0;
}

//Demo function
void conversation(int max_name_size)
{
	using namespace std;
	using namespace strvarken;

	StringVar your_name(max_name_size), our_name("Borg");


	cout << "What is your name ? \n";
	your_name.input_line(cin);
	cout << "We are " << our_name << endl;
	cout << "We will meet again " << your_name << endl;

	char* a;
	a = your_name.copy_piece(0, 2);
	cout << a << endl;

	char b;
	b = your_name.one_char(3);
	cout << b << endl;
	bool c = (your_name == our_name);
	cout << c << endl;

	your_name.set_char(2, 'a');
	cout << your_name << endl;

	your_name + our_name;
	cout << your_name << endl;

	StringVar d(30);
	cin >> d;
	cout << d << endl;

	system("pause");
}