#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <cstring>
#include "strvar.h"

using namespace std;

namespace strvarken
{
	//Uses cstddef and cstdlib
	StringVar::StringVar(int size) : max_length(size)
	{
		value = new char[max_length + 1];
		value[0] = '\0';
	}

	//Uses cstddef and cstdlib
	StringVar::StringVar() : max_length(100)
	{
		value = new char[max_length + 1];
		value[0] = '\0';
	}

	// Uses cstring, cstddef and cstdlib
	StringVar::StringVar(const char a[]) : max_length(strlen(a))
	{
		value = new char[max_length + 1];
		int b = strlen(a);
		for (int i = 0; i < b; i++)
		{
			value[i] = a[i];
		}
		value[strlen(a)] = '\0';
	}

	//Uses cstring, cstddef and cstdlib
	StringVar::StringVar(const StringVar& string_object) : max_length(string_object.length())
	{
		value = new char[max_length + 1];
		int b = strlen(string_object.value);
		for (int i = 0; i < b; i++)
		{
			value[i] = string_object.value[i];
		}
		value[strlen(string_object.value)] = '\0';
	}

	StringVar::~StringVar()
	{
		delete[] value;
	}

	//Uses cstring
	int StringVar::length() const
	{
		return strlen(value);
	}

	//Uses iostream
	void StringVar::input_line(istream& ins)
	{
		ins.getline(value, max_length + 1);
	}

	//Uses iostream
	ostream& operator << (ostream& outs, const StringVar& the_string)
	{
		outs << the_string.value;
		return outs;
	}

	char* StringVar::copy_piece(int a, int b) {
		char *subbuff;
		subbuff = new char[b - a + 2];
		memcpy(subbuff, &value[a], b - a + 1);
		subbuff[b - a + 1] = '\0';
		return subbuff;
	}

	char StringVar::one_char(int a) {
		return value[a];
	}

	void StringVar::set_char(int a, char c) {
		value[a] = c;
	}

	bool StringVar::operator==(StringVar a) {
		int max, min;
		max_length = strlen(value);
		a.max_length = strlen(value);
		if (max_length < a.max_length) {
			max = a.max_length;
			min = max_length;
		}

		else {
			max = max_length;
			min = a.max_length;
		}
		for (int i = 0; i <min; i++) {
			if (value[i] != NULL && a.value[i] != NULL && value[i] != a.value[i])
				return false;
		}
		for (int j = max - 1; j >= min; j++) {
			if (max_length < max && a.value[j] != ' ')
				return false;
			else if (a.max_length < max && value[j] != ' ')
				return false;
		}
		return true;
	}

	void StringVar :: operator +(StringVar a) {
		char *b;
		b = new char[strlen(value) + strlen(a.value) + 1];
		int c = strlen(value), d = strlen(a.value);
		for (int i = 0; i < c + d + 1; i++) {
			if (i < c)
				b[i] = value[i];
			else {
				b[i] = a.value[i - c];
			}
		}
		value = b;
	}

	istream &operator >> (istream &input, StringVar &Var)
	{
		input >> Var.value;
		return input;
	}





}//strvarken